﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Biblioteka
{
    public class Baza
    {
        public List<Dictionary<int, Podstawowa>> Słowniki = new List<Dictionary<int, Podstawowa>>();

        public Dictionary<int, Podstawowa> Core = new Dictionary<int, Podstawowa>()
            {
                {0, new Driads()},{1,new EternalGuard()},{2,new GladeGuard()},{3,new GladeRiders()}
            };
        public Dictionary<int, Podstawowa> Heroes = new Dictionary<int, Podstawowa>()
            {
                {0, new Drycha()},{1,new NaestraArahan()},{2,new GladeCaptain()},{3, new BattleStandardBearer()},
                {4, new Spellsinger()},{5, new Shadowdancer()}, {6, new Waystalker()}, {7, new Branchwraith()}
            };
        public Dictionary<int, Podstawowa> Lords = new Dictionary<int, Podstawowa>()
            {
                {0, new Orion()},{1,new Durthu()},{2, new Araloth()},{3, new GladeLord()},
                {4, new Spellweaver()},{5, new TreemanAncient()}
            };
        public Dictionary<int, Podstawowa> Rare = new Dictionary<int, Podstawowa>()
            {
                {0, new GreatEagles()},{1, new Waywatchers()},{2,new Treeman()}
            };
        public Dictionary<int, Podstawowa> Special = new Dictionary<int, Podstawowa>()
            {
                {0, new WildwoodRangers()},{1, new Wardancers()},{2, new TreeKin()}, {3, new DeepwoodScouts()},
                {4, new WarhawkRiders()},{5, new SistersOfTheThorn()},{6, new WildRiders()}
            };
        public Baza()
        {
            Słowniki.Add(Lords);
            Słowniki.Add(Heroes);
            Słowniki.Add(Core);
            Słowniki.Add(Special);
            Słowniki.Add(Rare);
        }

    }
}
