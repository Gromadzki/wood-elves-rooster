﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;


    public class Opcje
    {
        protected int koszt;
        protected string nazwa;
        protected bool czyWielokrotny;
        protected bool czyWybrany;
        
        public Opcje(int Koszt, string Nazwa, bool CzyWielokrotny)
        {
            koszt = Koszt;
            nazwa = Nazwa;
            czyWielokrotny = CzyWielokrotny;
            czyWybrany = false;
        }
        public int Koszt()
        { return koszt; }
        public bool CzyWybrany()
        { return czyWybrany; }
        public bool CzyWielokrotny()
        { return czyWielokrotny; }

    }
