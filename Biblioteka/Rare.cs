﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Biblioteka
{
    public class Waywatchers : Podstawowa
    {
        public Waywatchers()
            : base("Waywatchers", 5, 20)
        {
            listaOnOFF.Add(new Opcje(10, "Waywatcher Sentinel", false));
        }
    }
    public class GreatEagles : Podstawowa
    {
        public GreatEagles()
            : base("Great Eagles", 1, 50)
        { }
    }
    public class Treeman : Podstawowa
    {
        public Treeman()
            : base("Treeman", 1, 225,1)
        {
            listaOnOFF.Add(new Opcje(20, "Strangleroots", false));
        }
    }

}
