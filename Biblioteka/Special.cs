﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Biblioteka
{
    public class WildwoodRangers : Podstawowa
    {
        public WildwoodRangers()
            : base("Wildwood Rangers", 10, 11)
        {
            listaOnOFF.Add(new Opcje(10, "Wildwood Warden", false));
            listaOnOFF.Add(new Opcje(10, "Musican", false));
            listaOnOFF.Add(new Opcje(10, "Standard Bearer", false));
        }
    }
    public class Wardancers : Podstawowa
    {
        public Wardancers()
            : base("Wardancers", 5, 15)
        {
            listaOnOFF.Add(new Opcje(10, "Bladesinger", false));
            listaOnOFF.Add(new Opcje(10, "Musican", false));
            listaOnOFF.Add(new Opcje(1, "Asari spears", true));
        }
    }
    public class TreeKin : Podstawowa
    {
        public TreeKin()
            : base("Tree Kin", 3, 45)
        {
            listaOnOFF.Add(new Opcje(10, "Tree Kin Elder", false));
        }
    }
    public class DeepwoodScouts : Podstawowa
    {
        public DeepwoodScouts()
            : base("Deepwood Scouts", 5, 13)
        {
            listaOnOFF.Add(new Opcje(10, "Master Scout", false));
            listaOnOFF.Add(new Opcje(10, "Musican", false));
            listaOnOFF.Add(new Opcje(10, "Standard Bearer", false));
            listaWyborów.Add(new List<Opcje> {new Opcje(3, "Hagbaane tips", true),new Opcje(3, "Truefight arrows", true),
                                                new Opcje(4,"Moonfire shot", true), new Opcje(4,"Starfire shafts", true),
                                                new Opcje(4, "Swiftshiver Shards", true), new Opcje(5, "Arcane Bodkins", true)});
        }
    }
    public class WarhawkRiders : Podstawowa
    {
        public WarhawkRiders()
            : base("Warhawk Riders", 3, 45)
        {
            listaOnOFF.Add(new Opcje(10, "Wind Rider", false));
        }
    }
    public class SistersOfTheThorn : Podstawowa
    {
        public SistersOfTheThorn()
            : base("Sisters of the Thorn", 5, 26)
        {
            listaOnOFF.Add(new Opcje(10, "Handmaiden of the Thorn", false));
            listaOnOFF.Add(new Opcje(10, "Musican", false));
            listaOnOFF.Add(new Opcje(10, "Standard Bearer", false));
        }
    }
    public class WildRiders : Podstawowa
    {
        public WildRiders()
            : base("Wild Riders", 5, 26)
        {
            listaOnOFF.Add(new Opcje(10, "Wild Hunter", false));
            listaOnOFF.Add(new Opcje(10, "Musican", false));
            listaOnOFF.Add(new Opcje(10, "Standard Bearer", false));
            listaOnOFF.Add(new Opcje(2, "Shields", true));
        }
    }

}
