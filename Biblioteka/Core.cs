﻿using System;
using System.Collections.Generic;
using System.Net;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Documents;
using System.Windows.Ink;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Shapes;


namespace Biblioteka
{
    public class Driads : Podstawowa
    {
        public Driads()
            : base("Dryads", 10, 11)
        {
            listaOnOFF.Add(new Opcje(10, "Branch Nymph", false));
        }
    }
    public class EternalGuard : Podstawowa
    {
        public EternalGuard()
            : base("Eternal Guard", 10, 11)
        {
            listaOnOFF.Add(new Opcje(10, "Eternal Warden", false));
            listaOnOFF.Add(new Opcje(10, "Musican", false));
            listaOnOFF.Add(new Opcje(10, "Standard Bearer", false));
            listaOnOFF.Add(new Opcje(1, "Shields", true));
        }
    }
    public class GladeGuard : Podstawowa
    {
        public GladeGuard()
            : base("Glade Guard", 10, 12)
        {
            listaOnOFF.Add(new Opcje(10, "Lord's Bowman", false));
            listaOnOFF.Add(new Opcje(10, "Musican", false));
            listaOnOFF.Add(new Opcje(10, "Standard Bearer", false));
            listaWyborów.Add(new List<Opcje> {new Opcje(3, "Hagbaane tips", true),new Opcje(3, "Truefight arrows", true),
                                                new Opcje(4,"Moonfire shot", true), new Opcje(4,"Starfire shafts", true),
                                                new Opcje(4, "Swiftshiver Shards", true), new Opcje(5, "Arcane Bodkins", true)});

        }
    }
    public class GladeRiders : Podstawowa
    {
        public GladeRiders()
            : base("Glade Riders", 5, 19)
        {
            listaOnOFF.Add(new Opcje(10, "Glade Knight", false));
            listaOnOFF.Add(new Opcje(10, "Musican", false));
            listaOnOFF.Add(new Opcje(10, "Standard Bearer", false));
            listaWyborów.Add(new List<Opcje> {new Opcje(3, "Hagbaane tips", true),new Opcje(3, "Truefight arrows", true),
                                                new Opcje(4,"Moonfire shot", true), new Opcje(4,"Starfire shafts", true),
                                                new Opcje(4, "Swiftshiver Shards", true), new Opcje(5, "Arcane Bodkins", true)});

        }
    }

}
