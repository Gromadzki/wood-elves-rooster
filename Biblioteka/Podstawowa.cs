﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

    public class Podstawowa
    {
        protected string nazwa;
        protected int ilość;
        protected int ilośćMinimalna;
        protected int ilośćMaksymalna;
        protected int koszt; //za sztuke
        protected List<Opcje> listaOnOFF = new List<Opcje>();
        protected List<List<Opcje>> listaWyborów = new List<List<Opcje>>();

        public Podstawowa(string Nazwa, int IlośćMinimalna, int Koszt)
        {
            nazwa = Nazwa;
            ilość = IlośćMinimalna;
            ilośćMinimalna = IlośćMinimalna;
            koszt = Koszt;
            ilośćMaksymalna = 999;
        }
        public Podstawowa(string Nazwa, int IlośćMinimalna, int Koszt, int IlośćMaksymalna)
        {
            nazwa = Nazwa;
            ilość = IlośćMinimalna;
            ilośćMinimalna = IlośćMinimalna;
            koszt = Koszt;
            ilośćMaksymalna = IlośćMaksymalna;
        }
        public int PodajKoszt() //TODO listawyborów
        {
            int kosztKońcowy = ilość * koszt;
            foreach (var item in listaOnOFF)
            {
                if (item.CzyWybrany())
                {
                    if (item.CzyWielokrotny())
                    { koszt =+ item.Koszt() * ilość; }
                    else
                    {koszt =+ item.Koszt();}
                }
            }
            return kosztKońcowy ;
        }
        public void Zwiększ()
        {
            ilość++;
        }
        public void Zmniejsz()
        {
            if (ilość>ilośćMinimalna)
            {
                ilość--;
            }
        }
        public string Nazwa()
        {
            return nazwa;
        }
    }

