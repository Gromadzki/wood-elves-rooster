﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Biblioteka
{
    public class Drycha : Podstawowa
    {
        public Drycha()
            : base("Drycha", 1, 255,1)
        {
            
        }
    }
    public class NaestraArahan : Podstawowa
    {
        public NaestraArahan()
            : base("Naestra & Arahan", 1, 275, 1)
        {
            listaOnOFF.Add(new Opcje(220,"Ceithin-Har",false));
        }
    }
    public class GladeCaptain : Podstawowa
    {
        public GladeCaptain()
            : base("Glade Captain", 1, 75, 1)
        {
            listaOnOFF.Add(new Opcje(2,"Shield",false));
            listaWyborów.Add(new List<Opcje> {new Opcje(3, "Hagbaane tips", true),new Opcje(3, "Truefight arrows", true),
                                                new Opcje(4,"Moonfire shot", true), new Opcje(4,"Starfire shafts", true),
                                                new Opcje(4, "Swiftshiver Shards", true), new Opcje(5, "Arcane Bodkins", true)});
            listaWyborów.Add(new List<Opcje> {new Opcje(2, "Asrai spear", false),new Opcje(2, "Additional hand weapon", false),
                                                new Opcje(4,"Great weapon", false)});
            listaWyborów.Add(new List<Opcje> {new Opcje(10, "Elven Steed", false),new Opcje(50, "Great Eagle", false),
                                                new Opcje(65,"Great Stag", false)});
        }
    }
    public class BattleStandardBearer : Podstawowa
    {
        public BattleStandardBearer()
            : base("Battle Standard Bearer", 1, 100, 1)
        {
            listaWyborów.Add(new List<Opcje> {new Opcje(2, "Asrai spear", false),new Opcje(2, "Additional hand weapon", false),
                                                new Opcje(4,"Great weapon", false)});
            listaWyborów.Add(new List<Opcje> {new Opcje(10, "Elven Steed", false),new Opcje(50, "Great Eagle", false),
                                                new Opcje(65,"Great Stag", false)});
        }
    }
    public class Spellsinger : Podstawowa
    {
        public Spellsinger()
            : base("Spellsinger", 1, 80, 1)
        {
            listaWyborów.Add(new List<Opcje> {new Opcje(10, "Elven Steed", false),new Opcje(50, "Great Eagle", false),
                                                new Opcje(65,"Unicorn", false)});
            listaOnOFF.Add(new Opcje(35, "Level 2 Wizard", false));
            listaOnOFF.Add(new Opcje(5, "Asrai longbow", false));
        }
    }
    public class Shadowdancer : Podstawowa
    {
        public Shadowdancer()
            : base("Shadowdancer", 1, 100, 1)
        {

            listaOnOFF.Add(new Opcje(60, "Level 1 Wizard", false));

        }
    }
    public class Waystalker : Podstawowa
    {
        public Waystalker()
            : base("Waystalker", 1, 90, 1)
        {

        }
    }
    public class Branchwraith : Podstawowa 
    {
        public Branchwraith()
            : base("Brandwraith", 1, 75, 1)
        { }
    }
}
