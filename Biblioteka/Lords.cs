﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Biblioteka
{
    public class Orion : Podstawowa
    {
        public Orion()
            : base("Orion", 1, 600, 1)
        {
            listaOnOFF.Add(new Opcje(20,"Hounds of Orion",false));
        }
    }
    public class Durthu : Podstawowa
    {
        public Durthu()
            : base("Durthu", 1, 385, 1)
        { }
    }
    public class Araloth : Podstawowa
    {
        public Araloth()
            : base("Araloth", 1, 260, 1)
        { }
    }
    public class GladeLord : Podstawowa
    {
        public GladeLord()
            : base("Glade Lord", 1, 145, 1)
        {
            listaOnOFF.Add(new Opcje(3, "Shield", false));
            listaWyborów.Add(new List<Opcje> {new Opcje(3, "Hagbaane tips", true),new Opcje(3, "Truefight arrows", true),
                                                new Opcje(4,"Moonfire shot", true), new Opcje(4,"Starfire shafts", true),
                                                new Opcje(4, "Swiftshiver Shards", true), new Opcje(5, "Arcane Bodkins", true)});
            listaWyborów.Add(new List<Opcje> {new Opcje(3, "Asrai spear", false),new Opcje(3, "Additional hand weapon", false),
                                                new Opcje(6,"Great weapon", false)});
            listaWyborów.Add(new List<Opcje> {new Opcje(20, "Elven Steed", false),new Opcje(50, "Great Eagle", false),
                                                new Opcje(65,"Great Stag", false),new Opcje(200,"Forest Dragon",false)});
        }
    }
    public class Spellweaver : Podstawowa
    {
        public Spellweaver()
            : base("Spellweaver", 1, 185, 1)
        {
            listaWyborów.Add(new List<Opcje> {new Opcje(20, "Elven Steed", false),new Opcje(50, "Great Eagle", false),
                                                new Opcje(65,"Unicorn", false)});
            listaOnOFF.Add(new Opcje(35, "Level 4 Wizard", false));
            listaOnOFF.Add(new Opcje(5, "Asrai longbow", false));
        }
    }
    public class TreemanAncient : Podstawowa
    {
        public TreemanAncient()
            : base("Treeman Ancient", 1, 290, 1)
        {
            listaWyborów.Add(new List<Opcje>{new Opcje(0,"Level 2 Wizard", false),new Opcje(35,"Level 3 Wizard",false),
                                             new Opcje(70,"Level 4 Wizard",false)});
            listaOnOFF.Add(new Opcje(20, "Strangleroots", false));

        }
    }
}
