﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Navigation;
using Microsoft.Phone.Controls;
using Microsoft.Phone.Shell;
using Biblioteka;

namespace PhoneApp1
{
    public partial class Page2 : PhoneApplicationPage
    {
        public string stg="";
        public Page2()
        {
            InitializeComponent();
            strona.Text = stg;
            
        }
        protected override void OnNavigatedTo(NavigationEventArgs e)
        {
            base.OnNavigatedTo(e);
            if (NavigationContext.QueryString.TryGetValue("msg", out stg))
            {
                strona.Text = stg;
            }
                
        }
        
    }
}