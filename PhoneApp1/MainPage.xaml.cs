﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Navigation;
using Microsoft.Phone.Controls;
using Microsoft.Phone.Shell;
using System.Collections.ObjectModel;
using System.Windows.Media;
using Biblioteka;

namespace PhoneApp1
{
    public partial class Page1 : PhoneApplicationPage
    {

        List<List<Podstawowa>> Listy = new List<List<Podstawowa>>();
        Baza Słowniki = new Baza();
        
        public Page1()
        {
            InitializeComponent();

            Listy.Add(new List<Podstawowa>()); //Lords      0
            Listy.Add(new List<Podstawowa>()); //Hero       1
            Listy.Add(new List<Podstawowa>()); //Core       2
            Listy.Add(new List<Podstawowa>()); //Special    3
            Listy.Add(new List<Podstawowa>()); //Rare       4
            UpdateAll();

            foreach (var item in Słowniki.Lords)
            {
                WyborLords.Items.Add(item.Value.Nazwa());
            }
            foreach (var item in Słowniki.Heroes)
            {
                WyborHeroes.Items.Add(item.Value.Nazwa());
            }
            foreach (var item in Słowniki.Core)
            {
                WyborCore.Items.Add(item.Value.Nazwa());
            }
            foreach (var item in Słowniki.Special)
            {
                WyborSpecial.Items.Add(item.Value.Nazwa());
            }
            foreach (var item in Słowniki.Rare)
            {
                WyborRare.Items.Add(item.Value.Nazwa());
            }
        }
        #region eventy
        private void AddLords_Tap(object sender, System.Windows.Input.GestureEventArgs e)
        {
            Dodaj(WyborLords.SelectedIndex, 0);
            UpdateAll();
        }
        private void AddHeroes_Tap(object sender, System.Windows.Input.GestureEventArgs e)
        {
            Dodaj(WyborHeroes.SelectedIndex, 1);
            UpdateAll();
        }
        private void AddCore_Tap(object sender, System.Windows.Input.GestureEventArgs e)
        {
            Dodaj(WyborCore.SelectedIndex, 2);
            UpdateAll();
        }
        private void AddSpecial_Tap(object sender, System.Windows.Input.GestureEventArgs e)
        {
            Dodaj(WyborSpecial.SelectedIndex, 3);
            UpdateAll();
        }
        private void AddRare_Tap(object sender, System.Windows.Input.GestureEventArgs e)
        {
            Dodaj(WyborRare.SelectedIndex, 4);
            UpdateAll();
        }
        void tb_Tap(object sender, System.Windows.Input.GestureEventArgs e, string message)
        {
            NavigationService.Navigate(new Uri("/Page2.xaml?msg=" + message, UriKind.Relative));
        }
        private void reset_Tap(object sender, System.Windows.Input.GestureEventArgs e)
        {
            foreach (var item in Listy)
            {
                item.Clear();
            }
            UpdateAll();
        }
        #endregion
        #region metody

        public void Dodaj(int index, int grupa)
        {
            //Listy[grupa].add(Słowniki.Słowniki[grupa][index]);
            Dictionary<int,Podstawowa> tmp = Słowniki.Słowniki[grupa];
            Listy[grupa].Add(tmp[index]);
        }
       
        void UpdateAll()
        {
            UpdateLords();
            UpdateHeroes();
            UpdateCore();
            UpdateSpecial();
            UpdateRare();
        }
        void UpdateLords()
        {
            for (int i = Lords.Children.Count; i > 1; i--)
            {
                Lords.Children.RemoveAt(i - 1);
            }


            foreach (Podstawowa item in Listy[0])
            {
                StackPanel st = new StackPanel();
                st.Height = 80;
                TextBlock tb = new TextBlock();
                tb.Text = item.Nazwa();
                st.Children.Insert(0, tb);
                TextBlock pts = new TextBlock();
                pts.Text = item.PodajKoszt().ToString();
                st.Children.Insert(1, pts);
                tb.Tap += delegate(object sender, System.Windows.Input.GestureEventArgs e)
                {
                    //tb_Tap(sender, e, "text");
                    tb_Tap(sender, e, tb.Text);
                };

                Lords.Children.Insert(Lords.Children.Count, st);
            }
            PointsUpdate();
        }
        void UpdateHeroes()
        {
            for (int i = Heroes.Children.Count; i > 1; i--)
            {
                Heroes.Children.RemoveAt(i - 1);
            }


            foreach (Podstawowa item in Listy[1])
            {
                StackPanel st = new StackPanel();
                st.Height = 80;
                TextBlock tb = new TextBlock();
                tb.Text = item.Nazwa();
                st.Children.Insert(0, tb);
                TextBlock pts = new TextBlock();
                pts.Text = item.PodajKoszt().ToString();
                st.Children.Insert(1, pts);
                tb.Tap += delegate(object sender, System.Windows.Input.GestureEventArgs e)
                {
                    //tb_Tap(sender, e, "text");
                    tb_Tap(sender, e, tb.Text);
                };

                Heroes.Children.Insert(Heroes.Children.Count, st);
            }
            PointsUpdate();
        }
        void UpdateCore()
        {
            for (int i = Core.Children.Count; i >1 ; i--)
            {
                Core.Children.RemoveAt(i -1);
            }


            foreach (Podstawowa item in Listy[2])
            {
                StackPanel st = new StackPanel();
                st.Height = 80;
                TextBlock tb = new TextBlock();
                tb.Text = item.Nazwa();
                st.Children.Insert(0, tb);
                TextBlock pts = new TextBlock();
                pts.Text = item.PodajKoszt().ToString();
                st.Children.Insert(1, pts);
                tb.Tap += delegate(object sender, System.Windows.Input.GestureEventArgs e)
                {
                    //tb_Tap(sender, e, "text");
                    tb_Tap(sender, e, tb.Text);
                };
                
                Core.Children.Insert(Core.Children.Count, st);
            }
            PointsUpdate();
        }
        void UpdateSpecial()
        {
            for (int i = Special.Children.Count; i > 1; i--)
            {
                Special.Children.RemoveAt(i - 1);
            }
            foreach (Podstawowa item in Listy[3])
            {
                StackPanel st = new StackPanel();
                st.Height = 80;
                TextBlock tb = new TextBlock();
                tb.Text = item.Nazwa();
                st.Children.Insert(0, tb);
                TextBlock pts = new TextBlock();
                pts.Text = item.PodajKoszt().ToString();
                st.Children.Insert(1, pts);
                tb.Tap += delegate(object sender, System.Windows.Input.GestureEventArgs e)
                {
                    //tb_Tap(sender, e, "text");
                    tb_Tap(sender, e, tb.Text);
                };

                Special.Children.Insert(Special.Children.Count, st);
            }
            PointsUpdate();
        }
        void UpdateRare()
        {
            for (int i = Rare.Children.Count; i > 1; i--)
            {
                Rare.Children.RemoveAt(i - 1);
            }


            foreach (Podstawowa item in Listy[4])
            {
                StackPanel st = new StackPanel();
                st.Height = 80;
                TextBlock tb = new TextBlock();
                tb.Text = item.Nazwa();
                st.Children.Insert(0, tb);
                TextBlock pts = new TextBlock();
                pts.Text = item.PodajKoszt().ToString();
                st.Children.Insert(1, pts);
                tb.Tap += delegate(object sender, System.Windows.Input.GestureEventArgs e)
                {
                    //tb_Tap(sender, e, "text");
                    tb_Tap(sender, e, tb.Text);
                };

                Rare.Children.Insert(Rare.Children.Count, st);
            }
            PointsUpdate();
        }
        
        void PointsUpdate()
        {
            int sum = 0;
            int points =0;
            foreach (var item in Listy[0])
            {
                points = points + item.PodajKoszt();
            }
            sum = sum + points;
            LordsPoints.Text = "Points: " + points;
            points = 0;
            foreach (var item in Listy[1])
            {
                points = points + item.PodajKoszt();
            }
            sum = sum + points;
            HeroesPoints.Text = "Points: " + points;
            points = 0;
            foreach (var item in Listy[2])
            {
                points = points + item.PodajKoszt();
            }
            sum = sum + points;
            CorePoints.Text = "Points: " + points;
            points = 0;
            foreach (var item in Listy[3])
            {
                points = points + item.PodajKoszt();
            }
            sum = sum + points;
           SpecialPoints.Text = "Points: " + points;
           points = 0;
           foreach (var item in Listy[4])
           {
               points = points + item.PodajKoszt();
           }
           sum = sum + points;
           RarePoints.Text = "Points: " + points;
           SumOfPoints.Text = "Points:  " + sum;
        }

        #endregion

    }
}
